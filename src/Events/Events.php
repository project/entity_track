<?php

namespace Drupal\entity_track\Events;

/**
 * Contains all events thrown by Entity Track.
 */
final class Events {

  /**
   * Occurs when bulk tracking of all entities of an entity type is started.
   *
   * @var string
   */
  const BATCH_START = 'entity_track.batch_start';

}
