<?php

namespace Drupal\entity_track\Events;

use Drupal\Component\EventDispatcher\Event;

/**
 * Implementation of Entity Track batch start event.
 */
class EntityTrackBatchStartEvent extends Event {

  /**
   * The source entity type.
   *
   * @var string
   */
  protected $sourceEntityType;

  /**
   * EntityTrackPurgeEvent constructor.
   *
   * @param string $source_type
   *   The source entity type.
   */
  public function __construct($source_type = NULL) {
    $this->sourceEntityType = $source_type;
  }

  /**
   * Sets the source entity type.
   *
   * @param string $type
   *   The source entity type.
   */
  public function setSourceEntityType($type) {
    $this->sourceEntityType = $type;
  }

  /**
   * Gets the source entity type.
   *
   * @return null|string
   *   The source entity type or NULL.
   */
  public function getSourceEntityType() {
    return $this->sourceEntityType;
  }

}
