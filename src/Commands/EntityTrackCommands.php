<?php

namespace Drupal\entity_track\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_track\EntityTrackBatchManager;
use Drupal\entity_track\EntityTrackQueueBatchManager;
use Drush\Commands\DrushCommands;

/**
 * Entity Track drush commands.
 */
class EntityTrackCommands extends DrushCommands {

  /**
   * The Entity Track batch manager.
   *
   * @var \Drupal\entity_track\EntityTrackBatchManager
   */
  protected $batchManager;

  /**
   * The Entity track queue batch manager.
   *
   * @var \Drupal\entity_track\EntityTrackQueueBatchManager
   */
  protected $queueBatchManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity track configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $entityTrackConfig;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTrackBatchManager $batch_manager, EntityTrackQueueBatchManager $queue_batch_manager, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, $database) {
    parent::__construct();
    $this->batchManager = $batch_manager;
    $this->queueBatchManager = $queue_batch_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTrackConfig = $config_factory->get('entity_track.settings');
    $this->database = $database;
  }

  /**
   * Recreate tracking information.
   *
   * @command entity-track:recreate
   * @aliases et-r,entity-track-recreate
   * @option use-queue
   *   Use a queue instead of a batch process to recreate tracking info. This
   *   means tracking information won't be accurate until all items in the queue
   *   have been processed by cron runs.
   * @option batch-size
   *   When --use-queue is used, the queue will be populated in a batch process
   *   to avoid memory issues. The --batch-size flag can be optionally used to
   *   specify the batch size, for example --batch-size=500.
   */
  public function recreate($options = ['use-queue' => FALSE, 'batch-size' => 0]) {
    if (!empty($options['batch-size']) && empty($options['use-queue'])) {
      $this->output()->writeln(t('The --batch-size option can only be used when the --use-queue flag is specified. Aborting.'));
      return;
    }

    $this->database->delete('queue')->condition('name', 'entity_track_regenerate_queue')->execute();

    if (!empty($options['use-queue'])) {
      $this->queueBatchManager->populateQueue($options['batch-size']);
      drush_backend_batch_process();
    }
    else {
      $this->batchManager->recreate();
      drush_backend_batch_process();
    }
  }

}
