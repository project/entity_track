<?php

namespace Drupal\Tests\entity_track\FunctionalJavascript;

use Drupal\Tests\entity_track\Traits\EntityTrackLastEntityQueryTrait;

/**
 * Tests blocking of tracking.
 *
 * @package Drupal\Tests\entity_track\FunctionalJavascript
 *
 * @group entity_track
 */
class BlockTrackingTest extends EntityTrackJavascriptTestBase {

  use EntityTrackLastEntityQueryTrait;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article'])->save();
    $config = \Drupal::configFactory()->getEditable('entity_track.settings');
    $config->set('track_enabled_base_fields', TRUE);
    $config->save();
  }

  /**
   * Tests the blocking of tracking using a hook.
   */
  public function testBlockTracking() {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    $numeric_tracking_manager = \Drupal::service('entity_track_test.numeric_manager');

    // Create a node which should be tracked.
    $this->drupalGet('/node/add/article');
    $page->fillField('title[0][value]', '123');
    $page->pressButton('Save');
    $session->wait(500);
    $this->saveHtmlOutput();
    $assert_session->pageTextContains('Article 123 has been created.');
    /** @var \Drupal\node\NodeInterface $node1 */
    $node = $this->getLastEntityOfType('node', TRUE);
    $this->assertCount(1, $numeric_tracking_manager->loadByEntity($node));

    // Create another node. This node should be blocked by the hook based on
    // its specific title. See entity_track_test_entity_track_block_tracking.
    $this->drupalGet('/node/add/article');
    $page->fillField('title[0][value]', '42');
    $page->pressButton('Save');
    $session->wait(500);
    $this->saveHtmlOutput();
    $assert_session->pageTextContains('Article 42 has been created.');
    /** @var \Drupal\node\NodeInterface $node1 */
    $node = $this->getLastEntityOfType('node', TRUE);
    $this->assertEmpty($numeric_tracking_manager->loadByEntity($node));
  }

}
