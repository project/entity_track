<?php

/**
 * @file
 * Hooks for the entity_track module.
 */

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;

/**
 * Allows modules to block a specific tracking record.
 *
 * Modules implementing this hook should return TRUE if the operation should
 * be blocked. Any other return value will be disregarded and the register
 * written to the database.
 *
 * @param \Drupal\Core\Entity\EntityInterface $target_id
 *   The target entity.
 */
function hook_entity_track_block_tracking(EntityInterface $entity) {
  // Only track the default revision.
  if ($entity instanceof RevisionableInterface) {
    if (!$entity->isDefaultRevision()) {
      return TRUE;
    }
  }
}

/**
 * @} End of "addtogroup hooks".
 */
